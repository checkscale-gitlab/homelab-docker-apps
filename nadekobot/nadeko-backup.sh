#!/bin/bash
#BUCKET_NAME=
#PREFIX=
#NADEKO_PATH=
#TOPIC_ARN=
#ERROR_MESSAGE=
#REGION=
#export AWS_SHARED_CREDENTIALS_FILE=
#export AWS_CONFIG_FILE=
source nadeko.env
aws s3 cp $NADEKO_PATH/data/NadekoBot.db s3://$BUCKET_NAME/$PREFIX/data/$(date +%F-%H%M)_NadekoBot.db || aws sns publish --topic-arn $TOPIC_ARN --message "$ERROR_MESSAGE" --region $REGION
